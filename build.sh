#!/bin/sh

set -e
. ./env.sh
cd -- 'image/'
docker build --tag="$IMAGE_QUALIFIED_NAME" .
docker save --output="$IMAGE_FILE_PATH" "$IMAGE_QUALIFIED_NAME"