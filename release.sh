#!/bin/sh

set -e
if [ -n "$CI_SERVER" ]; then
    . ./env.sh
    docker login -u 'gitlab-ci-token' -p "${CI_BUILD_TOKEN}" 'registry.gitlab.com'
    docker load --input="${IMAGE_FILE_PATH}"
    docker push "${IMAGE_QUALIFIED_NAME}"
    docker logout 'registry.gitlab.com'
fi